module.exports = async function(req, res, next) {
  let arrPassAPIs = [];

    // let arrPassAPIs = ['POST/v1/user/login', 'POST/v1/user/register', 'POST/mobi-v1/user/facebookauth',
    //   'POST/mobi-v1/user/phonenumberauth', 'GET/mobi-v1/department/index', 'GET/mobi-v1/category/index',
    //   'GET/mobi-v1/hospital/index', 'GET/mobi-v1/system_setting/info'];

  let result = false;
  let arrresult = []
  let controller = res.data.controller
  let version = res.data.version;
  let action = res.data.action;
  let method = res.data.method;
  let strcheck = res.data.method + '/' + version + '/' + controller +'/'+ action;
  let token = res.data.params.token;

  if (arrPassAPIs.indexOf(strcheck) == -1) {

    // Check if token is valid
    let decoded = rxu.validateToken(token, pri.userconf.secret); if (!decoded) { result = false; }
    res.data.decoded = decoded;

    // TODO: Remove this at production
    if (token && !decoded) {
        res.data.response({ status: -111, msg: rxError(-111) });
        return;
    }

    // // Check user & admin permission
    // let allowPaths = ['admin/v1', 'user/mobi-v1'];
    // if (allowPaths.indexOf(decoded.role + '/' + version) !== -1) {
    //     result = true;
    // }
    //
    // // Check other role permission
    // let role = await roleBus.findOneByParams({ code: decoded.role });
    // let userPermission = await permissionBus.findOneByParams({ controller: controller, action: action, method: method });
    //
    // if (!role || !userPermission) {
    //   result = false;
    // }
    //
    // let permissions = role.permission.split(',');
    // if (permissions.indexOf(userPermission.id.toString()) === -1) {
    //   result = false;
    // }
  }

  next();
}
