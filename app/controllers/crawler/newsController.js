/* global rxController, rxu */
let fs = require('fs')
let util = require('util')
let request = require('request')
let mongoose = require('mongoose');
let parser = require('xml2json')
var he = require('he');
let htmlDom = require('htmldom');
let downloader = require('image-downloader')
let bus   = {};
let rxDb  = require(global.baseapp + 'include/rxDatabase');
let rxdata = {}
let moment = require('moment')

const axios = require('axios').default;
const Agent = require('socks5-https-client/lib/Agent');

var tr = require('tor-request');

const cheerio = require('cheerio')
const htmlparser2 = require('htmlparser2');

let model = require(global.baseapp + 'models/newsModel');
let siteModel = require(global.baseapp + 'models/sitenewsModel');
let queueModel = require(global.baseapp + 'models/queueModel');

function controller (res) { this.res = res; rxController.call(this, res); rxdata = res.data }
util.inherits(controller, rxController)

function doRequest(options) {
  return new Promise(function (resolve, reject) {
    request(options, function (error, res, body) {
      if (!error && parseInt(res.statusCode) === 200) {
        resolve(body)
      } else {
        reject(error)
      }
    })
  })
}

function rxChangeSlug(x) {
  let str = x
  if (typeof(x) !== 'undefined') {
    str = str.toLowerCase()
    str = str.replace(/[’`–~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, '')
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str.replace(/đ/g, 'd')
    str = str.replace(/ + /g, ' ')
    str = str.replace(/ \- /g, '-')
    str = str.replace(/ /g, '-')
    str = str.replace(/[^\w\s\-]/gi, '')
    str = str.trim()
  }
  return str
}

function minTwoDigits(n) {
  return (n < 10 ? '0' : '') + n;
}

function parseDateTimestamp (realasedate, year) {
  if (isNaN(realasedate)) {
    let timecurrent = 0
    let date = new Date()
    let timestamp = Math.floor(date.getTime()/1000)

    let patt = new RegExp(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/);
    let patt1 = new RegExp(/^([1-9]|([012][0-9])|(3[01]))-([0]{0,1}[1-9]|1[012])-\d\d\d\d [012]{0,1}[0-9]:[0-6][0-9]$/)

    if (realasedate.indexOf('day ago') != -1 || realasedate.indexOf('days ago') != -1) {
      let datenumber = Number(realasedate.replace(' day ago','').replace(' days ago','').replace(' ',''))
      timecurrent = timestamp - (datenumber*86400)
    } else if (realasedate.indexOf('week ago') != -1 || realasedate.indexOf('weeks ago') != -1) {
      let datenumber = Number(realasedate.replace(' week ago','').replace(' weeks ago','').replace(' ',''))
      datenumber = datenumber || 1
      timecurrent = timestamp - (datenumber*86400*7)
    } else if (realasedate.indexOf('hours ago') != -1 || realasedate.indexOf('hour ago') != -1) {
      let hoursnumber = Number(realasedate.replace(' hours ago','').replace(' hour ago','').replace(' ',''))
      timecurrent = timestamp - (hoursnumber*3600)
    } else if (realasedate.indexOf('month ago') != -1 || realasedate.indexOf('months ago') != -1) {
      let hoursnumber = Number(realasedate.replace(' month ago','').replace(' months ago','').replace(' ',''))
      timecurrent = timestamp - (hoursnumber*86400*30)
    } else if (realasedate.indexOf(', ') != -1 && realasedate.indexOf('Thứ') != -1) {
      let arrrealasedate = realasedate.split(', ')
      if (arrrealasedate.length == 2) {
        let realasedate = arrrealasedate[1]
        try {
          let strdatetime = realasedate.split(' - ')
          let strdate = strdatetime[1].split('/')
          let strdatetmp = ''
          if (strdate[2]) {
            strdatetmp = strdate[2] + '-' + strdate[1] + '-' + strdate[0] + 'T' + strdatetime[0] + ':00Z'
            timecurrent = Math.floor((new Date(strdatetmp).getTime()) / 1000) - 25200
          } else {
            strdate = strdatetime[0].split('/')
            if (strdate[2]) {
              strdatetmp = strdate[2] + '-' + strdate[1] + '-' + strdate[0] + 'T' + strdatetime[1] + ':00Z'
              timecurrent = Math.floor((new Date(strdatetmp).getTime()) / 1000) - 25200
            }
          }
        } catch (err) {console.log(err)}
      } else if (arrrealasedate.length == 3) {
        let datetmp = arrrealasedate[1]
        let timetmp = arrrealasedate[2]
        if (patt.test(datetmp)) {
          // use formats dd-mmm-YYYY, dd/mmm/YYYY, dd.mmm.YYYY
          let arrdatemonth = []
          if (datetmp.indexOf('-') != -1) { arrdatemonth = datetmp.split('-') }
          if (datetmp.indexOf('.') != -1) { arrdatemonth = datetmp.split('.') }
          if (datetmp.indexOf('/') != -1) { arrdatemonth = datetmp.split('/') }
          if (arrdatemonth.length = 3) {

            let strdatetmp = ''
            if (timetmp) {
              strdatetmp = arrdatemonth[2] + '-' + arrdatemonth[1] + '-' + arrdatemonth[0] + 'T' + timetmp + ':00'
            }else {
              strdatetmp = arrdatemonth[1] + '-' + arrdatemonth[0] + '-' + arrdatemonth[2]
            }
            timecurrent = Math.floor((new Date(strdatetmp).getTime()) / 1000)
          }
        }
      }
    } else if (patt.test(realasedate)) {
      // use formats dd-mmm-YYYY, dd/mmm/YYYY, dd.mmm.YYYY
      let arrdatemonth = []
      if (realasedate.indexOf('-') != -1) { arrdatemonth = realasedate.split('-') }
      if (realasedate.indexOf('.') != -1) { arrdatemonth = realasedate.split('.') }
      if (realasedate.indexOf('/') != -1) { arrdatemonth = realasedate.split('/') }
      if (arrdatemonth.length = 3) {
        let strdatetmp = arrdatemonth[1] + '-' + arrdatemonth[0] + '-' + arrdatemonth[2]
        timecurrent = Math.floor((new Date(strdatetmp).getTime()) / 1000)
      }
    } else if (realasedate.toLowerCase().indexOf('tháng') != -1) {
      let months_viet = ['tháng giêng - tháng một - tháng 1', 'tháng hai - tháng 2', 'tháng ba - tháng 3', 'tháng tư - tháng 4', 'tháng năm - tháng 5', 'tháng sáu - tháng 6', 'tháng bảy - tháng 7', 'tháng tám - tháng 8', 'tháng chín - tháng 9', 'tháng mười - tháng 10', 'tháng mười một - tháng 11', 'tháng mười hai - tháng 12']
      let strmonth = -1
      let monthtmp = ''
      for (let imonth in months_viet) {
        let arrmonth = months_viet[imonth].split(' - ')
        for (let jmonth of arrmonth) {
          if (realasedate.toLowerCase().indexOf(jmonth) != -1) {
            strmonth = (Number(imonth) + 1)
            monthtmp = jmonth
            break
          }
        }
        if (strmonth != -1) {
          break
        }
      }
      if (strmonth != -1) {
        let year = ''
        if (realasedate.toLowerCase().indexOf(',') != -1) {
          year = realasedate.split(',')[1].replace(' ', '')
        } else {
          year = new Date().getFullYear();
        }
        let date = realasedate.toLowerCase().replace(monthtmp,'').replace(year,'').replace(',','').replace(/ /g,'')
        let strdatetmp = minTwoDigits(strmonth) + '-' + minTwoDigits(date)  + '-' + year
        timecurrent = Math.floor((new Date(strdatetmp).getTime()) / 1000)
      }
    } else {
      let months = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december']
      let months_short = ['jan', 'feb', 'mar', 'apr', 'may', 'jun', 'jul', 'aug', 'sep', 'oct', 'nov', 'dec']

      let strmonth = -1
      let monthtmp = ''
      for (let imonth in months) {
        let month = months[imonth]
        if (realasedate.toLowerCase().indexOf(month) != -1) {
          strmonth = (Number(imonth) + 1)
          monthtmp = month
          break
        }
      }
      if (strmonth == -1) {
        for (let imonth in months_short) {
          let month = months_short[imonth]
          if (realasedate.toLowerCase().indexOf(month) != -1) {
            strmonth = (Number(imonth) + 1)
            monthtmp = month
            break
          }
        }
      }
      if (strmonth != -1) {
        let year =  new Date().getFullYear();
        let date = realasedate.toLowerCase().replace(monthtmp,'').replace(year,'')
        let strdatetmp = minTwoDigits(strmonth) + '-' + minTwoDigits(date)  + '-' + year
        timecurrent = Math.floor((new Date(strdatetmp).getTime()) / 1000)
      }

      if (isNaN(timecurrent)) {
        try {
          let date = moment(realasedate).format()
          let timestamp = moment(date).format("X")
          timecurrent = timestamp
        } catch(e2) {}
      }
    }

    return timecurrent
  } else {
    if (realasedate && realasedate != '' && realasedate.indexOf('/') != -1) {
      let arrdate = realasedate.split('/')
      if (arrdate.length == 3) {
        let stryear = rxu.pad(Number(arrdate[2]))
        let strmonth = rxu.pad(Number(arrdate[1]))
        let strdate = rxu.pad(Number(arrdate[0]))
        let dateformat = stryear + '-' + strmonth + '-' + strdate + 'T00:00:00Z'
        let date = new Date(dateformat);
        let timestamp = (date.getTime()/1000) - 25200
        return timestamp
      } else {
        return 0
      }
    } else if (year && year != '') {
      let dateformat = year + '-01-01T00:00:00Z'
      let date = new Date(dateformat);
      let timestamp = (date.getTime()/1000) - 25200
      return timestamp
    } else {
      return 0
    }
  }
}

controller.prototype.POSTcreateSitenews = async function() {
  let params = this.params
  let data = rxu.select(params, siteModel.create_fields, siteModel.number_fields);

  let findsite = await siteModel.findOne({id_site: params.id_site});
  if (!findsite) {
    let object = await siteModel.create(data);
    this.cbSuccess(object)
  } else {
    let object = await siteModel.update({id_site: params.id_site}, data);
    this.cbSuccess(object)
  }
}

controller.prototype.createQueue = async function() {
  // Run queue list crawler
  let params = this.params
  let arrlink = []
  if (params.link && params.id_site && params.start && params.end) {
    let start = params.start || 1
    let end = params.end || 1
    let site = await siteModel.findOne({id_site: params.id_site});
    let pagestr = params.pagestr || '/page/'
    if (site && site.id_site) {
      for (let i = start; i <= end; i++) {
        arrlink.push({
          link: params.link + pagestr + i,
          id_site: params.id_site,
          page: i,
          site: site
        })
      }
    }
  }

  if (arrlink.length > 0) {
    let queue = await queueModel.insertMany(arrlink);
  }

  this.cbSuccess(arrlink)
}

controller.prototype.getParent = async function() {
  let sitenews = await queueModel.findOne({check: 0});

  if (sitenews) {
    let link = sitenews.link

    let option = {
      headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
      uri: link,
      method: 'GET'
    }
    let proxy = rxu.rxaget(sitenews, 'site.proxy', false)
    if (proxy) {
      let ip_proxy = rxu.rxaget(sitenews, 'site.ip_proxy', '')
      if (ip_proxy) {
        option = {
          url: link,
          headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
          strictSSL: true,
          agentClass: Agent,
          agentOptions: {
            socksHost: ip_proxy,
            socksPort: 1080
          },
          method: 'GET'
        }
      }
    }

    let html = await doRequest(option)

    let dom = htmlparser2.parseDOM(html, { withDomLvl1: true, normalizeWhitespace: false, xmlMode: true, decodeEntities: true });
    let $ = cheerio.load(dom)
    let arrNewsParent = []

    let element = rxu.rxaget(sitenews, 'site.meta_parent.element', '')
    let eletitle = rxu.rxaget(sitenews, 'site.meta_parent.eletitle', '')
    let elepublisher = rxu.rxaget(sitenews, 'site.meta_parent.elepublisher', '')
    let eledesc = rxu.rxaget(sitenews, 'site.meta_parent.eledesc', '')
    let elelink = rxu.rxaget(sitenews, 'site.meta_parent.elelink', '')
    let attLink = rxu.rxaget(sitenews, 'site.meta_parent.attLink', '')
    let elecate = rxu.rxaget(sitenews, 'site.meta_parent.elecate', '')
    let catedefault = rxu.rxaget(sitenews, 'site.meta_parent.catedefault', '')
    let eleimage = rxu.rxaget(sitenews, 'site.meta_parent.eleimage', '')
    let attImage = rxu.rxaget(sitenews, 'site.meta_parent.attImage', '')
    let eletime = rxu.rxaget(sitenews, 'site.meta_parent.eletime', '')

    if (element) {
      $(element).each(async (i, elem) => {
        // Parent
        let objParent = { title: '', publisherName: '', description: '', url: '', slug: '', categoryName: '', images: [], date: '' }

        // Get title
        try {
          objParent['title'] = (eletitle) ? $(elem).find(eletitle).text().trim() : ''
        } catch(e1) {
          console.log('Error title')
          console.log(e1)
        }

        // Get publisher
        objParent['publisherName'] = elepublisher

        // Get desc short
        try {
          objParent['description'] = (eledesc) ? $(elem).find(eledesc).text().trim() : ''
        } catch(e2) {
          console.log('Error Desc')
          console.log(e2)
        }

        // Get link url
        try {
          objParent['url'] = (elelink) ? $(elem).find(elelink).attr(attLink).trim()  : ''
          objParent['originalUrl'] = (elelink) ? $(elem).find(elelink).attr(attLink).trim()  : ''
        } catch(e3) {
          console.log('Error Link')
          console.log(e3)
        }

        // Get slug
        try {
          objParent['slug'] = (objParent['title'] != '') ? rxChangeSlug(objParent['title']+' '+elepublisher) : ''
        } catch(e4) {
          console.log('Error Slug')
          console.log(e4)
        }

        // Get category
        try {
          objParent['categoryName'] = (elecate) ? $(elem).find(elecate).text().trim() : catedefault
        } catch(e5) {
          console.log('Error Cate')
          console.log(e5)
        }

        // Get image (If exist)
        try {
          objParent['images'] = (eleimage) ? [$(elem).find(eleimage).attr(attImage).trim()]  : ''
        } catch(e6) {
          console.log('Error Image')
          console.log(e6)
        }

        // Get Time Post (If exist)
        try {
          let timecurrent = (eletime) ? $(elem).find(eletime).text().trim()  : ''
          objParent['date'] = parseDateTimestamp(timecurrent)
        } catch(e7) {
          console.log('Error Time')
          console.log(e7)
        }

        // Create id post (If exist)
        try {
          objParent['contentId'] = rxu.md5(objParent['slug'])
        } catch(e8) {
          console.log('Error Post Id')
          console.log(e8)
        }

        objParent['id_site'] = rxu.rxaget(sitenews, 'site.id_site', '')

        if (objParent['title']) {
          let data = rxu.select(objParent, model.create_fields, model.number_fields);
          let findnew = await model.findOne({contentId: objParent['contentId']});
          if (!findnew) {
            let object = await model.create(data);
            arrNewsParent.push(data)
          }
        }

      })
    }

    let updatelink = await queueModel.update({_id: sitenews._id}, {check: 1});

    this.cbSuccess()
  } else {
    this.cbSuccess()
  }
}

controller.prototype.getDetail = async function() {
  let arrnews = await model.find({checkdetail: 0}, {_id: 1, originalUrl: 1, id_site: 1}).limit(5);
  let arrResult = []
  if (arrnews && arrnews.length > 0) {
    for (let news of arrnews) {
      let link = news.originalUrl
      let findsite = await siteModel.findOne({id_site: news.id_site});

      let option = {
        headers: { 'content-type': 'application/html', 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
        uri: link,
        method: 'GET'
      }
      let proxy = rxu.rxaget(findsite, 'proxy', false)
      if (proxy) {
        let ip_proxy = rxu.rxaget(findsite, 'ip_proxy', '')
        if (ip_proxy) {
          option = {
            url: link,
            headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
            strictSSL: true,
            agentClass: Agent,
            agentOptions: {
              socksHost: ip_proxy,
              socksPort: 1080
            },
            method: 'GET'
          }
        }
      }
      let html = await doRequest(option)
      let dom = htmlparser2.parseDOM(html, { withDomLvl1: true, normalizeWhitespace: false, decodeEntities: true });
      let $ = cheerio.load(dom)
      let arrNewsParent = []

      let elecontent = rxu.rxaget(findsite, 'meta_child.elecontent', '')
      let eleauthor = rxu.rxaget(findsite, 'meta_child.eleauthor', '')
      let elecate = rxu.rxaget(findsite, 'meta_child.elecate', '')
      let eletime = rxu.rxaget(findsite, 'meta_child.eletime', '')
      let eleimage = rxu.rxaget(findsite, 'meta_child.eleimage', '')
      let attImage = rxu.rxaget(findsite, 'meta_child.attImage', '')
      let eletags = rxu.rxaget(findsite, 'meta_child.eletags', '')

      // Child
      let objChild = { content: '' }

      // Get content
      try {
        let content = (elecontent && $(elecontent).html()) ? he.decode($(elecontent).html().replace(/\t/gi,'').replace(/\r/gi,'')) : ''
        if (content) {
          objChild['content'] = content
        }
      } catch(e1) {
        console.log('Error content')
        console.log(e1)
      }

      // Get name author
      try {
        if (eleauthor) {
          let author = $(eleauthor).text().trim()
          if (author) {
            objChild['authorName'] = author
          }
        }
      } catch(e2) {
        console.log('Error author')
        console.log(e2)
      }

      // Get category
      try {
        if (elecate) {
          let cate = $(elecate).text().trim()
          if (cate) {
            objChild['categoryName'] = cate
          }
        }
      } catch(e3) {
        console.log('Error category')
        console.log(e3)
      }

      // Get time post news (If exist)
      try {
        if (eletime) {
          let time = parseDateTimestamp($(eletime).text().trim())
          if (time) {
            objChild['date'] = time
          }
        }
      } catch(e4) {
        console.log('Error time')
        console.log(e4)
      }

      // Get image (If exist)
      try {
        if (eleimage) {
          let img = $(eleimage).eq(0).attr(attImage)
          if (img) {
            objChild['images'] = [img]
          }
        }
      } catch(e5) {
        console.log('Error image')
        console.log(e5)
      }

      // Get tags (If exist)
      try {
        if (eletags) {
          // $(eletags)
          $(eletags).each((i, elem) => {
            let tag = $(elem).text().replace(/\t/gi,'').replace(/\r/gi,'').replace(/\n/gi,'')
            if (!objChild['tags']) {objChild['tags'] = []}
            objChild['tags'].push(tag)
          })
        }
      } catch(e6) {
        console.log('Error tags')
        console.log(e6)
      }

      objChild['checkdetail'] =  1
      let dataupdate = rxu.select(objChild, model.create_fields, model.number_fields);
      let updatenews = await model.update({_id: news._id}, dataupdate);
      arrResult.push(news._id)
    }

    // { content: '', author: '', category: '', tags: [], image: '', time: '' }
    // Remove ads, content another
    this.cbSuccess(arrResult)
  } else {
    this.cbSuccess()
  }
}

controller.prototype.getParentTest = async function() {
  // let link = 'https://doisongvnn.info/tin-moi/page/2'
  let params = this.params
  let link = rxu.rxaget(params, 'link', '')
  let page    = rxu.rxaget(params, 'page', '')
  let pageNumber    = rxu.rxaget(params, 'pageNumber', 1)
  let arrNewsParent = []

  let pageSize = 9500;
  while (link !== '') {
    if (pageSize === Number(pageNumber)) { break; }

    let link1 = ''
    if (pageSize !== 0) {
      link1 = link + page + pageSize
    } else {
      link1 = link
    }

      console.log(link1)

    let option = {
      headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
      uri: link1,
      method: 'GET'
    }

    let proxy = rxu.rxaget(params, 'proxy', false)
    if (proxy) {
      let ip_proxy = rxu.rxaget(params, 'ip_proxy', '')
      if (ip_proxy) {
        option = {
          url: link,
          headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
          strictSSL: true,
          agentClass: Agent,
          agentOptions: {
            socksHost: ip_proxy,
            socksPort: 1080
          },
          method: 'GET'
        }
      }
    }

    let html = await doRequest(option)

    let dom = htmlparser2.parseDOM(html, { withDomLvl1: true, normalizeWhitespace: false, xmlMode: true, decodeEntities: true });
    let $ = cheerio.load(dom)

    let element = rxu.rxaget(params, 'element', '')//'.block-wrap .module-latest.list-item-category'
    let eletitle = rxu.rxaget(params, 'eletitle', '')//'.post-title'
    let elepublisher = rxu.rxaget(params, 'elepublisher', '')//'Ngồi bút trẻ'
    let eledesc = rxu.rxaget(params, 'eledesc', '')//'.entry'
    let elelink = rxu.rxaget(params, 'elelink', '')//'.post-title a'
    let attLink = rxu.rxaget(params, 'attLink', '')//'href'
    let elecate = rxu.rxaget(params, 'elecate', '')//''
    let catedefault = rxu.rxaget(params, 'catedefault', '')//'Văn Hóa'
    let eleimage = rxu.rxaget(params, 'eleimage', '')//'.thumb-wrap a img'
    let attImage = rxu.rxaget(params, 'attImage', '')//'src'
    let eletime = rxu.rxaget(params, 'eletime', '')//'.meta-bar-wrap .date-meta'
    let eletimetext = rxu.rxaget(params, 'eletimetext', '')
    if (element) {
      $(element).each((i, elem) => {
        // Parent
        let objParent = { title: '', publisher: '', desc: '', link: '', slug: '', category: '', tags: '', image: '', time: '', timetext: '' }

        // Get title
        try {
          objParent['title'] = (eletitle) ? $(elem).find(eletitle).text().trim() : ''
        } catch(e1) {
          console.log('Error title')
          console.log(e1)
        }

        // // Get publisher
        // objParent['publisher'] = elepublisher
        //
        // // Get desc short
        // try {
        //   // objParent['desc'] = (eledesc) ? $(elem).find(eledesc).text().trim() : ''
        // } catch(e2) {
        //   console.log('Error Desc')
        //   console.log(e2)
        // }
        //
        // // Get link url
        // try {
        //   // objParent['link'] = (elelink) ? $(elem).find(elelink).attr(attLink).trim()  : ''
        // } catch(e3) {
        //   console.log('Error Link')
        //   console.log(e3)
        // }
        //
        // // Get slug
        // try {
        //   // objParent['slug'] = (objParent['title'] != '') ? rxChangeSlug(objParent['title']+' '+elepublisher) : ''
        // } catch(e4) {
        //   console.log('Error Slug')
        //   console.log(e4)
        // }
        //
        // // Get category
        // try {
        //   objParent['category'] = (elecate) ? $(elem).find(elecate).text().trim() : catedefault
        // } catch(e5) {
        //   console.log('Error Cate')
        //   console.log(e5)
        // }
        //
        // // Get image (If exist)
        // try {
        //   // objParent['image'] = (eleimage) ? $(elem).find(eleimage).attr(attImage).trim()  : ''
        // } catch(e6) {
        //   console.log('Error Image')
        //   console.log(e6)
        // }

        // Get Time Post (If exist)
        try {
          let timecurrent = (eletime) ? $(elem).find(eletime).text().trim()  : ''
          objParent['time'] = parseDateTimestamp(timecurrent)
        } catch(e7) {
          console.log('Error Time')
          console.log(e7)
        }

        // Get Time Post (If exist)
        try {
          let timecurrent = (eletimetext) ? $(elem).find(eletimetext).text().trim()  : ''
          objParent['timetext'] = timecurrent
        } catch(e7) {
          console.log('Error Time')
          console.log(e7)
        }

        // // Create id post (If exist)
        // try {
        //   // objParent['postid'] = rxu.md5(objParent['slug'])
        // } catch(e8) {
        //   console.log('Error Post Id')
        //   console.log(e8)
        // }

        // if (objParent['title']) {
          arrNewsParent.push(objParent)
        // }
        console.log(arrNewsParent.length)


      })
    }
    pageSize -= 1;
  }

  var counts = arrNewsParent.reduce((p, c) => {
  var name = c.timetext;
    if (!p.hasOwnProperty(name)) {
      p[name] = 0;
    }
    p[name]++;
    return p;
  }, {});

  console.log(counts);


 //  let initialValue = 0
 //
 //  let group = arrNewsParent.reduce((r, a) => {
 //   r[a.time] = [...r[a.time] || [], a];
 //   return r;
 // }, {});
 //
 //
 //  let totals = arrNewsParent.length
  this.cbSuccess(counts)
}

controller.prototype.getDetailTest = async function() {
  let params = this.params
  let link = rxu.rxaget(params, 'link', '')


  let option = {
    headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
    uri: link,
    method: 'GET'
  }
  let proxy = rxu.rxaget(params, 'proxy', false)
  if (proxy) {
    let ip_proxy = rxu.rxaget(params, 'ip_proxy', '')
    if (ip_proxy) {
      option = {
        url: link,
        headers: { 'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36' },
        strictSSL: true,
        agentClass: Agent,
        agentOptions: {
          socksHost: ip_proxy,
          socksPort: 1080
        },
        method: 'GET'
      }
    }
  }

  let html = await doRequest(option)
  let dom = htmlparser2.parseDOM(html, { withDomLvl1: true, normalizeWhitespace: false, decodeEntities: true });
  let $ = cheerio.load(dom)
  let arrNewsParent = []

  let elecontent = rxu.rxaget(params, 'elecontent', '')//'.entry'
  let eleauthor = rxu.rxaget(params, 'eleauthor', '')//''
  let elecate = rxu.rxaget(params, 'elecate', '')//''
  let eletime = rxu.rxaget(params, 'eletime', '')//'.time-meta span'
  let eleimage = rxu.rxaget(params, 'eleimage', '')//'.entry img'
  let attImage = rxu.rxaget(params, 'attImage', '')//'src'
  let eletags = rxu.rxaget(params, 'eletags', '')//'.tags a'

  // Child
  let objChild = { content: '' }

  // Get content
  try {
    let content = (elecontent && $(elecontent).html()) ? he.decode($(elecontent).html().replace(/\t/gi,'').replace(/\r/gi,'')) : ''
    if (content) {
      objChild['content'] = content
    }
  } catch(e1) {
    console.log('Error content')
    console.log(e1)
  }

  // Get name author
  try {
    if (eleauthor) {
      let author = $(eleauthor).text().trim()
      if (author) {
        objChild['author'] = author
      }
    }
  } catch(e2) {
    console.log('Error author')
    console.log(e2)
  }

  // Get category
  try {
    if (elecate) {
      let cate = $(elecate).text().trim()
      if (cate) {
        objChild['category'] = cate
      }
    }
  } catch(e3) {
    console.log('Error category')
    console.log(e3)
  }

  // Get time post news (If exist)
  try {
    if (eletime) {
      let time = parseDateTimestamp($(eletime).text().trim())
      if (time) {
        objChild['time'] = time
      }
    }
  } catch(e4) {
    console.log('Error time')
    console.log(e4)
  }

  // Get image (If exist)
  try {
    if (eleimage) {
      let img = $(eleimage).eq(0).attr(attImage)
      if (img) {
        objChild['image'] = img
      }
    }
  } catch(e5) {
    console.log('Error image')
    console.log(e5)
  }

  // Get tags (If exist)
  try {
    if (eletags) {
      // $(eletags)
      $(eletags).each((i, elem) => {
        let tag = $(elem).text().replace(/\t/gi,'').replace(/\r/gi,'').replace(/\n/gi,'')
        if (!objChild['tags']) {objChild['tags'] = []}
        objChild['tags'].push(tag)
      })
    }
  } catch(e6) {
    console.log('Error tags')
    console.log(e6)
  }

  // { content: '', author: '', category: '', tags: [], image: '', time: '' }
  // Remove ads, content another
  this.cbSuccess(objChild)
}

module.exports = controller;
