let chalk = require( "chalk" )
let cluster = require( "cluster" )
let os = require( "os" )
var moment     = require('moment');
var cron       = require('node-cron');

global.basedir = __dirname;
global.baseapp = global.basedir + '/app/';
global.baseup  = global.basedir + '/upload/';
global.baseup_img = global.baseup + 'image/';
global.baseup_file = global.baseup + 'file/';

global.pri            = require(global.baseapp + 'include/rxPrivate.js');
global.rxu            = require(global.baseapp + 'include/rxUtil.js');
global.rxController   = require(global.baseapp + 'include/rxController.js');
global.rxError        = require(global.baseapp + 'include/rxError.js');

// var User   = require(global.baseapp + 'models/userModel.js');
let maxCPU = process.env.cpu;

// SDK cron-jobs
let initCronJobs = function() {

};

let checkAdminAccount = function() {
    // User.findOne({ username: 'superadmin' }).exec((err, user) => {
    //     if (!user) {
    //         let users = [
    //             { username: 'admin', password: rxu.md5('admin@1234'), role: 'admin' },
    //             { username: 'superadmin', password: rxu.md5('admin@1234'), role: 'superadmin' }, ];
    //         User.insertMany(users);
    //         console.log('Success insert admins');
    //     }
    // });
};

// Master - fork thread
if ( cluster.isMaster ) {

    console.log( chalk.red( "[Cluster]" ), "Master process started.", process.pid );

    // SDK cron-jobs
    initCronJobs();

    // Check create admin account
    checkAdminAccount();

    // Create thread base on cores
    for ( let i = 0, coreCount = maxCPU || os.cpus().length ; i < coreCount ; i++ ) {
        let worker = cluster.fork();
    }

    // If worker exit , add new worker or handle it
    cluster.on('exit', function handleExit( worker, code, signal ) {

            // Show logs
            console.log( chalk.yellow( "[Cluster]" ), "Worker stop. ", worker.process.pid );
            console.log( chalk.yellow( "[Cluster]" ), "Worker die: ", worker.exitedAfterDisconnect );

            // If exception, try to restart worker
            if ( ! worker.exitedAfterDisconnect ) {

                // If this is loop exception we meet cpu consumtion, because master thread try to create exception worker thread
                let worker = cluster.fork()
            }

        }
    );

}
// Worker - do job
else {

    // Run worker
    console.log( chalk.red( "[Worker]" ), "Worker started.", process.pid );
    require( "./index.js" );
}
