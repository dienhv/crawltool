var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

// Category Types: home, news, video

// create a schema
var schema = new Schema({
  id:           { type: String, default: 'default' },
  name:         { type: String, default: '' },
  icon:         { type: String, default: '' },
  selected_icon:{ type: String, default: '' },
  types:        { type: [{ type: String, enums: ['news', 'home', 'video'] }], default: ['news'] },
  order:        { type: Number, default: 0 },
  portrait_image:{ type: String, default: '' },

  products:     { type: Number, default: 0 },

  is_deleted:   { type: Number, default: 0 },
  is_active:    { type: Number, default: 1 },

  created_at:   { type: Number, default: 0 },
  updated_at:   { type: Number, default: 0 }
}, { versionKey: false });

schema.plugin(mongoosePaginate);
schema.pre('save', function(next) {
    var self = this;
    if (!self.created_at) { self.created_at = Math.floor(Date.now() / 1000); }
    self.updated_at = Math.floor(Date.now() / 1000);
    next();
});

var model = mongoose.model('category', schema);

// Automatic determine type fields
model.all_fields        = Object.keys(schema.obj);
model.number_fields     = model.all_fields.filter(x => schema.obj[x].type==Number);
model.object_id_fields  = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.ObjectId);
model.json_fields       = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.Mixed);

// Pre-define type fields
model.unique_fields     = ['id'];

model.create_fields     = ['id', 'name', 'icon', 'is_active', 'types', 'icon', 'selected_icon', 'order', 'portrait_image'];
model.required_fields   = ['id', 'name'];
model.updating_fields   = ['id', 'name', 'icon', 'is_active', 'types', 'icon', 'selected_icon', 'products', 'order', 'portrait_image'];

module.exports = model;
