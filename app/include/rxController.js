var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');

function rxController(res) { this.res = res; this.params = res.data.params; }

rxController.prototype.validate = function(rules) {
    let validateResult = rxu.validate(this.params, rules);
    if (validateResult !== null) {
        throw { code: -110, message: rxError(-110), data: validateResult };
    };
};

rxController.prototype.pagingex = function(inputArr, params) {
    // Paging
    var pg_page = (typeof(params.pg_page) == 'undefined' || parseInt(params.pg_page) < 1) ? 1 : parseInt(params.pg_page)
    var pg_size = (typeof(params.pg_size) == 'undefined' || parseInt(params.pg_size) < 10 || parseInt(params.pg_size) > 1000) ? 10 : parseInt(params.pg_size)

    // Sorting
    var st_col = params.st_col || 'created_at'
    var st_type = (typeof(params.st_type) == 'undefined' || parseInt(params.st_type) != 1) ? '-' : ''
    var st_params = {};

    inputArr = inputArr.sort(dynamicSort(st_type + st_col))

    // Cut array by page_size and page_number
    startIndex = (pg_page - 1) * pg_size;
    inputArr = inputArr.slice(startIndex, startIndex + pg_size);

    return inputArr
}

rxController.prototype.preUpdate = function(editables, params, arr_inttype) {
    arr_inttype = arr_inttype || ['created_at', 'updated_at', 'is_deleted', 'is_active']

    var data_update = {}
    for (index in params) {
        if (editables.indexOf(index) > -1) {
            if (arr_inttype.indexOf(index) > -1) {
                params[index] = Number(params[index])
            }
            data_update[index] = params[index]
        }
    }

    return data_update
}

rxController.prototype.preventDupplicate = function(model, params, ignoreId, callback) {
    ignoreId = ignoreId || 0;

    // Ignore current data
    if (ignoreId) {
        params['_id'] = { '$ne': mongoose.Types.ObjectId(ignoreId) };
    }

    var rxdata = this.res.data;
    model.find(params, function(err, docs) {
        if (docs && docs.length) {
            rxdata.response({ status: -112, msg: rxError(-112) });
        } else {
            callback();
        }
    });

    return true;
}

rxController.prototype.cbSuccess = function(data, message) {
    var rxdata = this.res.data;
    rxdata.response({ status: 1, msg: message || "Success", data: data, exit: false });
};

rxController.prototype.cbFailed = function(message, data, code) {
    var rxdata = this.res.data;
    if (typeof message == 'string') {
        rxdata.response({ status: code || -1, msg: message || rxError(-1), data: data });
    } else if (typeof message == 'number') {
        rxdata.response({ status: message, msg: rxError(message) || rxError(-1), data: data });
    } else {
        rxdata.response({ status: -1, msg: rxError(-1), data: data });
    }
};

module.exports = rxController;
