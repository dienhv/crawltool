/* global pri */
// index.js
let cron = require('node-cron')
let request = require('request')
let host = 'http://localhost:' + pri.serverconf.port

let showstart = function (name) { console.log('\n--- S T A R T --- ' + name) }
let showend = function (name) { console.log('--- D O N E --- ' + name) }

cron.schedule('*/5 * * * * *', function () {
  let name = 'Automation Get Parent'
  showstart(name)
  request(host + '/crawler/news/getParent', (error, response, body) => {
    if (response && response.statusCode && body) {
      console.log(body)
    }
  })
  showend(name)
})

cron.schedule('*/10 * * * * *', function () {
  let name = 'Automation Get Detail'
  showstart(name)
  request(host + '/crawler/news/getDetail', (error, response, body) => {
    if (response && response.statusCode && body) {
      console.log(body)
    }
  })
  showend(name)
})

