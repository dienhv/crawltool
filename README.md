Danh sách API sử dụng
=

## 1. Định nghĩa tham số

### 1.1. Kết quả trả về

{status:@, msg:@, data: @}

- SUCCESS:       status = 1

### 1.2. Các loại account (role)
    'admin'        : Account quản trị
    'director'     : Account giám đốc
    'documentary'  : Account chứng từ

### 1.3. Các mã lỗi có thể trả vế

    '-1': 'Lỗi hệ thống',
    '-100': 'Tên đăng nhập không hợp lệ. Tên đăng nhập phải ít nhất 6 kí tự và nhiều nhất 24 kí tự, bao gồm chữ hoặc số.',
    '-101': 'Tên đăng nhập đã tồn tại',
    '-102': 'Mật khẩu không hợp lệ. Mật khẩu phải ít nhất 6 kí tự và nhiều nhất 50 kí tự, bao gồm chữ hoặc số.',
    '-103': 'Email không hợp lệ. Email ít nhất 6 kí tự và nhiều nhất 45 kí tự.',
    '-104': 'Email đã tồn tại.',
    '-105': 'Mã ứng dụng không tồn tại.',
    '-106': 'Số điện thoại không hợp lệ.',
    '-107': 'Người dùng không tồn tại.',
    '-108': 'Sai mật khẩu',
    '-109': 'Sai tên đăng nhập hoặc mật khẩu.',
    '-110': 'Dữ liệu đầu vào không hợp lệ.',
    '-111': 'Không có quyền truy cập.',
    '-112': 'Dữ liệu trùng',
    '-113': 'Không tìm thấy dữ liệu yêu cầu.',
    '-114': 'Không có quyền chỉnh sửa.',

### 1.4. Paging, Sort, Filter

    pg_page: Số page
    pg_size: Giới hạn mỗi page, tối đa 1000
    st_type: 1 or -1
    st_col: field cần sort
    search_field_name: Với field_name là trường cần tìm kiếm

## 2. Danh sách API Web

### 2.1. Thống kê

### 2.1.1 Lấy thông tin thống kê

Đường dẫn: `/statistics/index`

Phương thức: `GET`

Dữ liệu đầu vào:

    start_date_s: thời điểm bắt đầu (đơn vị s)
    end_date_s: thời điểm kết thúc (đơn vị s)
    department_id: filter theo department (optional)
    user_id: filter theo user (optional)

Kết quả trả về:

    {
        "total_users": <Tổng số user trong hệ thống>,
        "new_users": <Số user mới>,
        "number_of_orders": <Số lượng order thành công>,
        "total_revenue": <Tổng doanh thu>
    }
