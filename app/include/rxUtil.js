var util       = require('util')
var mongoose   = require('mongoose')
var moment     = require('moment')
var validate   = require(global.baseapp + 'include/rxValidate.js')
var crypto     = require('crypto')
var CryptoJS   = require('crypto-js');
var jwt        = require('jsonwebtoken')
var request    = require('request')

function rxUtil() {}
var rxu = rxUtil;

rxu.shortUUID = function() {
    var now = new Date();
    return Math.floor(Math.random() * 10) + parseInt(now.getTime()).toString(36).toUpperCase();
};

rxu.md5 = function(strsource) {
  var passHash = crypto.createHash('md5').update(strsource).digest("hex")
  return passHash
}

rxu.genhex = function() {
  var newToken = crypto.randomBytes(64).toString('hex')
  return newToken
}

rxu.genstr = function(length, source) {
  length = length || 8
  source = source || '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'

  var result = ''
  for (var i = length; i > 0; --i) result += source[Math.floor(Math.random() * source.length)]

  return result
}

rxu.now = function(unit) {
  if (unit == 'ms') { return Date.now(); }
  return Math.floor(Date.now() / 1000);
}

rxu.validate = function(params, rules) {
  var returnSum  = true
  var returnData = {}

  for(index in rules) {
    var tempResult = validate.value(params[index], rules[index])
    if (!tempResult['approved']) {
      returnData[index] = []
      tempResult.each(function(err) { returnData[index].push(err) })
      returnSum = false
    }
  }

  return !returnSum ? returnData : null;
}

rxu.isDupplicate = async function(model, params, ignoreId) {
    ignoreId = ignoreId || 0;

    // Ignore current data
    if (ignoreId) {
        params['_id'] = { '$ne': mongoose.Types.ObjectId(ignoreId) };
    }

    let objc = await model.findOne(params);

    return objc ? true : false;
}

rxu.signToken = function(data, secret) {
    return jwt.sign(data, secret/*{expiresIn: pri.userconf.tokenExpiresIn}*/);
};

rxu.signTokenNoExpired = function(data, secret) {
    return jwt.sign(data, secret);
};

rxu.validateToken = function(token, secret) {
    try {
        return jwt.verify(token, secret);
    } catch(err) {
        // console.log(err);
        return null;
    }
};

rxu.select = function(params, allFields, intFields) {
    var newData = {}; allFields = allFields || []; intFields = intFields || [];
    for (let i = 0; i < allFields.length; i++) {
        let field = allFields[i];
        if (params[field] !== undefined) {
            if (intFields.indexOf(field) > -1) {
                newData[field] = isNaN(Number(params[field])) ? 0 : Number(params[field]);
            } else {
                newData[field] = params[field];
            }
        }
    }
    return newData;
};

rxu.filter = function(params, arr_inttype, arr_idtype) {
    // Int type field
    arr_inttype = arr_inttype || ['created_at', 'updated_at', 'is_deleted', 'is_active'];

    // Id type field
    arr_idtype = arr_idtype || [];

    // Default filter first
    var filter_default = { is_deleted: 0 };

    for (var index in params) {
        let curEle = params[index]; if (curEle === '' || curEle === undefined) { continue }

        // Search range
        if (index.indexOf('search_range_gte_') === 0) {
          let curSearchTerm = index.replace('search_range_gte_', '')
          filter_default[curSearchTerm] = filter_default[curSearchTerm] || {}
          filter_default[curSearchTerm]['$gte'] = Number(curEle) || 0
        } else if (index.indexOf('search_range_lt_') === 0) {
          let curSearchTerm = index.replace('search_range_lt_', '')
          filter_default[curSearchTerm] = filter_default[curSearchTerm] || {}
          filter_default[curSearchTerm]['$lte'] = Number(curEle)+60 || 0

          // Search text
        } else if (index.indexOf('search_text') === 0) {
          filter_default['$text'] = {}
          filter_default['$text']['$search'] = `\"${curEle}\"`
        } else if (index.indexOf('search_') === 0) {
          let curSearchTerm = index.replace('search_', '')
          if (arr_inttype.indexOf(curSearchTerm) > -1) {
            filter_default[curSearchTerm] = Number(curEle) || 0
          } else if (arr_idtype.indexOf(curSearchTerm) > -1) {
            filter_default[curSearchTerm] = mongoose.Types.ObjectId(curEle)
          } else {
            filter_default[curSearchTerm] = new RegExp(curEle, 'i')
          }
        }

        if (index.indexOf('searchdate_') != -1) {
          filter_default['created_at'] = { "$gte": Number(params['searchdate_fromDate']), "$lt": Number(params['searchdate_toDate']) }
        }
        if (index.indexOf('searcharr_') != -1) {
          var curSearchArr = index.replace('searcharr_', '');
          var arrvalue = curEle.split(",")
          filter_default[curSearchArr] = { $in: arrvalue }
        }
        if (index.indexOf('searchninarr_') != -1) {
          var curSearchArr = index.replace('searchninarr_', '');
          var arrvalue = curEle.split(",")
          filter_default[curSearchArr] = { $nin: arrvalue }
        }
        if (index.indexOf('searchObjId_') != -1) {
          let curSearchObjTerm = index.replace('searchObjId_', '')
          filter_default[curSearchObjTerm] = mongoose.Types.ObjectId(curEle)
        }
        if (index.indexOf('searchfilter_inventory') == 0) {
          if (curEle == 1) {
            filter_default['$expr'] = { $gt: [ '$inventorylevel' , '$amount' ] }
          } else {
            filter_default['$expr'] = { $gt: [ '$amount', '$inventorylevel' ] }
          }
        }
    }

    return filter_default;
};

rxu.paging = function(params) {
    // Paging
    var pg_page = (typeof(params.pg_page) == 'undefined' || parseInt(params.pg_page) < 1) ? 1 : parseInt(params.pg_page)
    var pg_size = (typeof(params.pg_size) == 'undefined' || parseInt(params.pg_size) > 1000 || parseInt(params.pg_size) < 1) ? 10 : parseInt(params.pg_size)

    // Sorting
    var st_col = params.st_col || 'created_at'
    var st_type = (typeof(params.st_type) == 'undefined' || parseInt(params.st_type) != 1) ? -1 : 1
    var st_params = {};
    st_params[st_col] = st_type

    return [ { $sort: st_params }, { $skip: (pg_page - 1) * pg_size }, { $limit: pg_size } ];
};

Date.prototype.yyyymmdd = function() {
   var mm = this.getMonth() + 1;
   var dd = this.getDate();

   return [this.getFullYear(),
       (mm > 9 ? '-' : '-0') + mm,
       (dd > 9 ? '-' : '-0') + dd
   ].join('');
};

rxu.nArray = function(n) {
    return Array.from(new Array(n),(val,index)=>index+1);
}

rxu.date = function(interval_ms, format) {
    return moment(interval_ms).format(format || 'HH:mm DD/MM/YYYY');
}

rxu.change_alias = function(alias) {
    var str = alias;
    if (str) {
      str = str.toLowerCase();
      str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, "a");
      str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, "e");
      str = str.replace(/ì|í|ị|ỉ|ĩ/g, "i");
      str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, "o");
      str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, "u");
      str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, "y");
      str = str.replace(/đ/g, "d");
      str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, "A");
      str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, "E");
      str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, "I");
      str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, "O");
      str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, "U");
      str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, "Y");
      str = str.replace(/Đ/g, "D");  
    }
    return str;
}

rxu.rxChangeSlug = function (x) {
  let str = x
  if (typeof(x) !== 'undefined') {
    str = str.toLowerCase()
    str = str.replace(/[`~!@#$%^&*()_|+=?;:'",.<>{}[\]\\/]/gi, '')
    str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a')
    str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e')
    str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i')
    str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o')
    str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u')
    str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y')
    str = str.replace(/đ/g, 'd')
    str = str.replace(/ + /g, ' ')
    str = str.replace(/ \- /g, '-')
    str = str.replace(/ /g, '-')
    str = str.trim()
  }
  return str
}

rxu.rxaget = function(variable, keys, defaultVal) {
  defaultVal = defaultVal || "";
  let resultVal = defaultVal;

  try {
    // Keys is array of index
    if (Array.isArray(keys)) {
      let tempResult = variable;
      for (let i in keys) {
        tempResult = tempResult[keys[i]];
      }
      resultVal = tempResult;

      // Keys is a string
    } else {
      keys = keys.split(".");
      let tempResult = variable;
      for (let i in keys) {
        tempResult = tempResult[keys[i]];
      }
      resultVal = tempResult;
    }

    // Case exception, access undefined variable
  } catch (e) {
    resultVal = defaultVal;
  }

  // Case undefined after all
  if (resultVal === undefined) {
    resultVal = defaultVal;
  }

  // HAPPYENDING
  return resultVal;
}

rxu.doRequest = function(options) {
  return new Promise(function (resolve, reject) {
    request(options, function (error, res, body) {
      if (!error && parseInt(res.statusCode) === 200) {
        resolve(body)
      } else {
        reject(error)
      }
    })
  })
}


module.exports = rxu
