var path       = require('path');
var url        = require('url');
var formidable = require('formidable');

let DEFAULT_VERSION = 'v1';
let BASEUP_PATH = global.baseup + 'temp/';

module.exports = function(req, res, next) {
  // console.log('enrichRequest')

  var data = {};
  data.decoded = {};
  data.req = req;
  data.url = url;
  data.startTime  = rxu.now('ms');

  // Bind data to res
  res.data = data;
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Content-Type', 'application/json');
  res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, X-Access-Token, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials");

  // Request related
  data.end        = false;
  data.hostname   = req.headers.host;
  data.pathinfo   = url.parse(req.url, true);
  data.pathname   = data.pathinfo.pathname;
  data.allpath    = data.pathname.split('/');
  data.version    = data.allpath[1];
  data.method     = req.method;
  data.controller = (typeof(data.allpath[2]) !== 'undefined') ? data.allpath.slice(2, data.allpath.length-1).join('/') : 'index';
  data.action     = (typeof(data.allpath[3]) !== 'undefined') ? data.allpath[data.allpath.length-1] : 'index';

  if (data.allpath.length < 4) {
    if (typeof(data.allpath[1]) !== 'undefined' && data.allpath[1] == 'crawler') {
      data.version    = (typeof(data.allpath[1]) !== 'undefined') ? data.allpath[1] : 'index';
      data.controller = (typeof(data.allpath[2]) !== 'undefined') ? data.allpath[2] : 'index';
      data.action     = (typeof(data.allpath[3]) !== 'undefined') ? data.allpath[3] : 'index';
    } else {
      data.version    = DEFAULT_VERSION
      data.controller = (typeof(data.allpath[1]) !== 'undefined') ? data.allpath[1] : 'index';
      data.action     = (typeof(data.allpath[2]) !== 'undefined') ? data.allpath[2] : 'index';  
    }
  }

  data.response   = function(options) {
    options  = options || {};

    let tempData = {};

    tempData.status = options.status || 1;
    tempData.msg     = options.msg     || 'Request success!';
    tempData.data    = options.data    || {};
    tempData.cpu     = (rxu.now('ms') - data.startTime).toString().substr(0, 8) + 'ms';
    let returnData   = JSON.stringify(tempData);

    if (data.end == true) { return; }
    data.end = true;

    res.writeHead(200);
    res.end(returnData);
  };

  // Request params
  let form = new formidable.IncomingForm(); form.encoding = 'utf-8';

  if (data.method === 'GET') {
    data.params = decodeQueryParams(data.pathinfo.query); next();
  } else {
    form.uploadDir = BASEUP_PATH;
    form.parse(req, function(err, fields, files) {
      data.files = files; data.form = form;
      // Add get + post params
      data.params = Object.assign(fields, decodeQueryParams(data.pathinfo.query));

      next();
    });
  }
}

let decodeQueryParams = function(params) {
    let result = {};
    for (let key in params) {
        result[key] = decodeURIComponent(params[key]);
    }
    return result;
}
