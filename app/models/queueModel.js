var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

// Category Types: home, news, video

// create a schema
var schema = new Schema({
  link:       { type: String, default: '' },
  id_site:    { type: String, default: '' },
  page:       { type: String, default: '' },
  site:       { type: Schema.Types.Mixed, default: {} },
  check:      { type: Number, default: 0 },
}, { versionKey: false });

schema.plugin(mongoosePaginate);
schema.pre('save', function(next) {
    var self = this;
    if (!self.created_at) { self.created_at = Math.floor(Date.now() / 1000); }
    self.updated_at = Math.floor(Date.now() / 1000);
    next();
});

var model = mongoose.model('queue', schema);

// Automatic determine type fields
model.all_fields        = Object.keys(schema.obj);
model.number_fields     = model.all_fields.filter(x => schema.obj[x].type==Number);
model.object_id_fields  = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.ObjectId);
model.json_fields       = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.Mixed);

// Pre-define type fields
model.unique_fields     = [];

model.create_fields     = ["link","id_site","page","site","check"];
model.required_fields   = [];
model.updating_fields   = ["link","id_site","page","site","check"];

module.exports = model;
