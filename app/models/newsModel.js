var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

// Category Types: home, news, video

// create a schema
var schema = new Schema({
  id_site:        { type: String, default: ''},
  contentId:      { type: String, default: ''},
  title:          { type: String, default: ''},
  contentTypes:   { type: Number, default: 1  },
  url:            { type: String, default: ''},
  originalUrl:    { type: String, default: ''},
  publisherId:    { type: String, default: ''},
  publisherName:  { type: String, default: ''},
  publisherIcon:  { type: String, default: ''},
  categoryId:     { type: String, default: ''},
  categoryName:   { type: String, default: ''},
  categoryNameArr: { type: Array, default: [] },
  avatarUrl:      { type: String, default: ''},
  description:    { type: String, default: ''},
  content:        { type: String, default: ''},
  avatarWidth:    { type: String, default: ''},
  avatarHeight:   { type: String, default: ''},
  totalComments:  { type: String, default: ''},
  authorName:     { type: String, default: ''},
  date:           { type: Number, default: 0  },
  tags:           { type: Array, default: [] },
  redirect:       { type: Number, default: 0  },
  images:         { type: Array, default: [] },
  stt_import:     { type: Number, default: 0  },
  source:         { type: String, default: ''},
  uuid:           { type: String, default: ''},
  type_error:     { type: String, default: ''},
  date_review:    { type: String, default: ''},
  violateError:   { type: String, default: ''},
  explainError:   { type: String, default: ''},
  violents:       { type: Schema.Types.Mixed, default: { adverse: '', violent: 0, violentinfo: 0, violentstatus: 0, violentimgs: [] } },
  violentdesc:    { type: String, default: ''},
  field:          { type: String, default: ''},
  title_post:     { type: String, default: ''},
  link_post:      { type: String, default: ''},
  author_post:    { type: String, default: ''},
  name_publisher: { type: String, default: ''},
  link_publisher: { type: String, default: ''},
  info_publisher: { type: String, default: ''},
  date_post:      { type: String, default: ''},
  history_violent_publisher: { type: String, default: ''},
  violenthistory: { type: [Schema.Types.Mixed], default: [] },
  level_measure:  { type: Number, default: 0 },
  tags_import:    { type: Array, default: [] },
  enterprise_name: { type: String, default: ''},
  note:           { type: String, default: ''},
  content_import: { type: String, default: ''},
  ads_post:       { type: String, default: ''},
  legal_grounds:  { type: String, default: ''},
  sourceName:     { type: String, default: ''},
  typepost:       { type: String, default: ''},
  credo:          { type: String, default: ''},
  categorymain:   { type: String, default: ''},
  sheetName:      { type: String, default: ''},
  name_user:      { type: String, default: ''},
  name:           { type: String, default: ''},
  id:             { type: String, default: ''},
  checking:       { type: Number, default: 0 },
  theme_group:    { type: String, default: ''},
  topic:          { type: String, default: ''},
  theme_group_id: { type: String, default: ''},
  tempViolent:    { type: String, default: ''},
  mainViolent:    { type: String, default: ''},
  checkmainViolent: { type: Number, default: 0 },
  datatemp:       { type: Array, default: [] },
  domain:         { type: Number, default: 0 },
  checkerror:     { type: Number, default: 0 },
  checkmainpost:  { type: Number, default: 0 },
  is_real:        { type: Number, default: 0 },
  is_done:        { type: Number, default: 0 },
  is_donenew:     { type: Number, default: 0 },
  is_use:         { type: Number, default: 0 },
  checkdetail:    { type: Number, default: 0 },

  is_deleted:     { type: Number, default: 0 },
  is_active:      { type: Number, default: 1 },
  crawled_at:     { type: Number, default: 0 },
  created_at:     { type: Number, default: 0 },
  updated_at:     { type: Number, default: 0 }
}, { versionKey: false });

schema.plugin(mongoosePaginate);
schema.pre('save', function(next) {
    var self = this;
    if (!self.created_at) { self.created_at = Math.floor(Date.now() / 1000); }
    self.updated_at = Math.floor(Date.now() / 1000);
    next();
});

var model = mongoose.model('postnews', schema);

// Automatic determine type fields
model.all_fields        = Object.keys(schema.obj);
model.number_fields     = model.all_fields.filter(x => schema.obj[x].type==Number);
model.object_id_fields  = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.ObjectId);
model.json_fields       = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.Mixed);

// Pre-define type fields
model.unique_fields     = [];

model.create_fields     = ['id_site', 'checkdetail', 'contentId', 'title', 'contentTypes', 'url', 'originalUrl', 'publisherId', 'publisherName', 'publisherIcon', 'categoryId', 'categoryName', 'categoryNameArr', 'avatarUrl', 'description', 'content', 'avatarWidth', 'avatarHeight', 'totalComments', 'authorName', 'date', 'tags', 'redirect', 'images', 'stt_import', 'source', 'uuid', 'type_error', 'date_review', 'violateError', 'explainError', 'violents', 'violentdesc', 'field', 'title_post', 'link_post', 'author_post', 'name_publisher', 'link_publisher', 'info_publisher', 'date_post', 'history_violent_publisher', 'violenthistory', 'level_measure', 'tags_import', 'enterprise_name', 'note', 'content_import', 'ads_post', 'legal_grounds', 'sourceName', 'typepost', 'credo', 'categorymain', 'sheetName', 'name_user', 'name', 'id', 'checking', 'theme_group', 'topic', 'theme_group_id', 'tempViolent', 'mainViolent', 'checkmainViolent', 'datatemp', 'domain', 'checkerror', 'checkmainpost', 'is_real', 'is_done', 'is_donenew', 'is_use', 'is_deleted', 'is_active', 'crawled_at', 'created_at', 'updated_at'];
model.required_fields   = [];
model.updating_fields   = ['id_site', 'checkdetail', 'contentId', 'title', 'contentTypes', 'url', 'originalUrl', 'publisherId', 'publisherName', 'publisherIcon', 'categoryId', 'categoryName', 'categoryNameArr', 'avatarUrl', 'description', 'content', 'avatarWidth', 'avatarHeight', 'totalComments', 'authorName', 'date', 'tags', 'redirect', 'images', 'stt_import', 'source', 'uuid', 'type_error', 'date_review', 'violateError', 'explainError', 'violents', 'violentdesc', 'field', 'title_post', 'link_post', 'author_post', 'name_publisher', 'link_publisher', 'info_publisher', 'date_post', 'history_violent_publisher', 'violenthistory', 'level_measure', 'tags_import', 'enterprise_name', 'note', 'content_import', 'ads_post', 'legal_grounds', 'sourceName', 'typepost', 'credo', 'categorymain', 'sheetName', 'name_user', 'name', 'id', 'checking', 'theme_group', 'topic', 'theme_group_id', 'tempViolent', 'mainViolent', 'checkmainViolent', 'datatemp', 'domain', 'checkerror', 'checkmainpost', 'is_real', 'is_done', 'is_donenew', 'is_use', 'is_deleted', 'is_active', 'crawled_at', 'created_at', 'updated_at'];

module.exports = model;