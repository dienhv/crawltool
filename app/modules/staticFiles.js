var path       = require('path');
var fs         = require('fs');

module.exports = function(request, response, next) {
  // console.log('staticFiles')

  let filePath = '.' + request.url
  if (filePath == './') filePath = './index.html'

  let isStatic = (filePath.indexOf('upload/image') !== -1) ? true : false;
  let extname = path.extname(filePath)
  let contentType = 'text/html'
  switch (extname.toLowerCase()) {
      case '.txt':
        isStatic = true
        contentType = 'text/html'
        break;
      case '.html':
        isStatic = true
        contentType = 'text/html'
        break;
      case '.ico':
        isStatic = true
        contentType = 'image/x-icon'
        break
      case '.png':
        isStatic = true
        contentType = 'image/png'
        break
      case '.jpeg':
      case '.jpg':
        isStatic = true
        contentType = 'image/jpeg'
        break
      case '.webp':
        isStatic = true
        contentType = 'image/webp'
        break
      case '.wav':
        isStatic = true
        contentType = 'audio/wav'
        break
      default:
        break
  }

  if (isStatic) {
    response.setHeader("Cache-Control", "public, max-age=2592000")
    response.setHeader("Expires", new Date(Date.now() + 2592000000).toUTCString())
    fs.readFile(filePath, function(error, content) {
      if (error) {
        if(error.code == 'ENOENT'){
          fs.readFile('./404.html', function(error, content) {
            response.writeHead(200, { 'Content-Type': contentType })
            response.end(content, 'utf-8')
          });
        }
        else {
          response.writeHead(500)
          response.end('Sorry, check with the site admin for error: '+error.code+' ..\n')
          response.end()
        }
      }
      else {
        response.writeHead(200, { 'Content-Type': contentType })
        response.end(content, 'utf-8')
      }
    })
  } else {
    next();
  }
}
