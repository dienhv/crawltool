'use strict';

/**
 * Module dependencies.
 * @private
 */

 let logger          = require('./modules/requestLogger')
 let enrichRequest   = require('./modules/enrichRequest');
 let staticFiles     = require('./modules/staticFiles');

var http            = require('http');

var app = exports = module.exports = {};

app.init = function init() {
  this.cache = {};
  this.engines = {};
  this.settings = {};
  this.plugins = [enrichRequest, staticFiles, logger];

  this.defaultConfiguration();

  return app;
};

app.defaultConfiguration = function defaultConfiguration() {
  this.env = process.env.NODE_ENV || 'development';
  this.mountpath = './';
};

app.handle = function handle(req, res) {
  try {
    app.handleRecursive(0, req, res);
  } catch(err) {
    console.log(err);
  }
};

app.handleRecursive = async function(index, req, res) {
  if (index < 0 || index >= this.plugins.length) { return; }

  try {
    await this.plugins[index](req, res, function() {
      app.handleRecursive(index+1, req, res)
    });
  } catch(err) {
    res.data.response({status: -1, msg: 'Not support'});
    console.log(err);
  }
}

app.use = function(handler) {
  this.plugins.push(handler);
};

app.pre = function(handler) {
  this.plugins.unshift(handler);
};

app.post = function(handler) {
  this.plugins.push(handler);
};

app.listen = function listen() {
  var server = http.createServer(handleHttpRequest);
  server.timeout = 5000000;
  return server.listen.apply(server, arguments);
};

function handleHttpRequest(req, res) {
  app.handle(req, res);
}

function logerror(err) {
  /* istanbul ignore next */
  if (this.get('env') !== 'test') console.error(err.stack || err.toString());
}
