var mongoose = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');
var Schema = mongoose.Schema;

// Category Types: home, news, video

// create a schema
var schema = new Schema({
  id_site:      { type: String, default: '' },
  proxy:        { type: Boolean, default: false},
  ip_proxy:     { type: String, default: '' },
  name:         { type: String, default: '' },
  site_url:     { type: String, default: '' },
  meta_parent:  { type: Schema.Types.Mixed, default: { element: '', eletitle: '', elepublisher: '', eledesc: '', elelink: '', attLink: '', elecate: '', catedefault: '', eleimage: '', attImage: '', eletime: '' } },
  meta_child:   { type: Schema.Types.Mixed, default: { elecontent: '', eleauthor: '', elecate: '', eletime: '', eleimage: '', attImage: '', eletags: '' } },
  
  is_deleted:   { type: Number, default: 0 },
  is_active:    { type: Number, default: 1 },
  created_at:   { type: Number, default: 0 },
  updated_at:   { type: Number, default: 0 }
}, { versionKey: false });

schema.plugin(mongoosePaginate);
schema.pre('save', function(next) {
    var self = this;
    if (!self.created_at) { self.created_at = Math.floor(Date.now() / 1000); }
    self.updated_at = Math.floor(Date.now() / 1000);
    next();
});

var model = mongoose.model('sitenews', schema);

// Automatic determine type fields
model.all_fields        = Object.keys(schema.obj);
model.number_fields     = model.all_fields.filter(x => schema.obj[x].type==Number);
model.object_id_fields  = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.ObjectId);
model.json_fields       = model.all_fields.filter(x => schema.obj[x].type==mongoose.Schema.Types.Mixed);

// Pre-define type fields
model.unique_fields     = [];

model.create_fields     = ["id_site","proxy","ip_proxy","name","site_url","meta_parent","meta_child","is_deleted","is_active","created_at","updated_at"];
model.required_fields   = [];
model.updating_fields   = ["id_site","proxy","ip_proxy","name","site_url","meta_parent","meta_child","is_deleted","is_active","created_at","updated_at"];

module.exports = model;
