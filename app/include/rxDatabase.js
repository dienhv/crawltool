var mongoose   = require('mongoose');
var mongoosePaginate = require('mongoose-paginate');

// Custom promise
mongoose.Promise = global.Promise;

// Init one time
//rxu.mgo = mongoose.connect('mongodb://gametop_admin:U6C760XNOQnRRQ==@203.113.159.120/gametop')
let mgo = mongoose.connect('mongodb://127.0.0.1/' + global.pri.mgoconf.db, { useMongoClient: true });
mongoose.connection.once('open', function() {
    console.log("Connect to mongo successfully!");
}).on('error', function() {
    console.error("Connect mongo failed!");
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
  mongoose.connection.close(function () {
    console.log('Mongoose disconnected through app termination');
    process.exit(0);
  });
});

let rxDatabase = {};

rxDatabase.filter = function(hmodel, params, arr_inttype, arr_idtype) {
    // Int type field
    arr_inttype = arr_inttype || ['created_at', 'updated_at', 'is_deleted', 'is_active'];

    // Id type field
    arr_idtype = arr_idtype || [];

    // Default filter first
  let filter_default = { is_deleted: 0 }

  for (let index in params) {
    let curEle = params[index]; if (curEle === '' || curEle === undefined) { continue }
    // Search range
    if (index.indexOf('search_range_gte_') === 0) {
      let curSearchTerm = index.replace('search_range_gte_', '')
      filter_default[curSearchTerm] = filter_default[curSearchTerm] || {}
      filter_default[curSearchTerm]['$gte'] = Number(curEle) || 0
    } else if (index.indexOf('search_range_lt_') === 0) {
      let curSearchTerm = index.replace('search_range_lt_', '')
      filter_default[curSearchTerm] = filter_default[curSearchTerm] || {}
      filter_default[curSearchTerm]['$lt'] = Number(curEle) || 0

      // Search text
    } else if (index.indexOf('search_text') === 0) {
      filter_default['$text'] = {}
      filter_default['$text']['$search'] = curEle
    } else if (index.indexOf('search_') === 0) {
      let curSearchTerm = index.replace('search_', '')
      if (arrInttype.indexOf(curSearchTerm) > -1) {
        filter_default[curSearchTerm] = Number(curEle) || 0
      } else if (arrIdtype.indexOf(curSearchTerm) > -1) {
        filter_default[curSearchTerm] = mongoose.Types.ObjectId(curEle)
      } else {
        filter_default[curSearchTerm] = new RegExp(curEle, 'i')
      }
    }

    if (index.indexOf('searchdate_') != -1) {
      filter_default['created_at'] = { "$gte": Number(params['searchdate_fromDate']), "$lt": Number(params['searchdate_toDate']) }
    }
    if (index.indexOf('searchfilter_inventory') == 0) {
      if (curEle == 1) {
        filter_default['$expr'] = { $gt: [ '$inventorylevel' , '$amount' ] }
      } else {
        filter_default['$expr'] = { $gt: [ '$amount', '$inventorylevel' ] }
      }
    }
  }

    return hmodel.find(filter_default);
};

rxDatabase.paging = function(hmodel, params, populate, filters) {

    // Paging
    var pg_page = (typeof(params.pg_page) == 'undefined' || parseInt(params.pg_page) < 1) ? 1 : parseInt(params.pg_page)
    var pg_size = (typeof(params.pg_size) == 'undefined' || parseInt(params.pg_size) > 1000 || parseInt(params.pg_size) < 1) ? 10 : parseInt(params.pg_size)

    // Sorting
    var st_col = params.st_col || 'created_at'
    var st_type = (typeof(params.st_type) == 'undefined' || parseInt(params.st_type) != 1) ? -1 : 1
    var st_params = {};
    st_params[st_col] = st_type

    let filterParams = rxu.filter(params, hmodel.number_fields, hmodel.object_id_fields);
    let finalParams = Object.assign(filterParams, filters || {});
    return hmodel.find(finalParams).sort(st_params).limit(pg_size).skip((pg_page - 1) * pg_size).populate(populate || '').select('-password');
    // return hmodel.paginate(finalParams, { offset: (pg_page - 1) * pg_size, limit: pg_size, sort: st_params, populate: populate || '', select: '-password' });
}

rxDatabase.pagingTotal = function(hmodel, params, populate, filters) {

    // Paging
    var pg_page = (typeof(params.pg_page) == 'undefined' || parseInt(params.pg_page) < 1) ? 1 : parseInt(params.pg_page)
    var pg_size = (typeof(params.pg_size) == 'undefined' || parseInt(params.pg_size) > 1000 || parseInt(params.pg_size) < 1) ? 10 : parseInt(params.pg_size)

    // Sorting
    var st_col = params.st_col || 'created_at'
    var st_type = (typeof(params.st_type) == 'undefined' || parseInt(params.st_type) != 1) ? -1 : 1
    var st_params = {};
    st_params[st_col] = st_type

    let filterParams = rxu.filter(params, hmodel.number_fields, hmodel.object_id_fields);
    let finalParams = Object.assign(filterParams, filters || {});
    return hmodel.paginate(finalParams, { offset: (pg_page - 1) * pg_size, limit: pg_size, sort: st_params, populate: populate || '', select: '-password' });
}

module.exports = rxDatabase;
