module.exports = async function(req, res, next) {
  // console.log('requestHandler')

  try {
    let controllerFile = global.baseapp + 'controllers' + '/' + res.data.version + '/' + res.data.controller + 'Controller.js';
    let actionName = (req.method == 'GET') ? res.data.action : req.method + res.data.action;
    let controllerObj = new(require(controllerFile))(res);

    await controllerObj[actionName]();
    next();
  } catch(err) {
    console.log(err)
    next();
    // if (global.rxaget(err,'code') == 'MODULE_NOT_FOUND' || global.rxaget(err,'message') == 'controllerObj[actionName] is not a function') {
    //   // console.log(err);
    //   res.data.response({ status: -1, msg: 'Not support:' + global.rxaget(err,'message')});
    // } else {
    //   res.data.response({ status: global.rxaget(err,'code'), msg: global.rxaget(err,'msg') || rxError(global.rxaget(err,'code')), data: global.rxaget(err,'data') });
    //   console.log(err);
    // }
  }
}
