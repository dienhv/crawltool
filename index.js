// Catch unhandle exception & rejection
process.on('unhandledRejection', (err) => { console.log('Caught rejection: ' + err + ' line: '+ err.stack) } )
process.on('uncaughtException',  (err) => { console.log('Caught exception: ' + err + ' line: '+ err.stack) } )

global.basedir = __dirname
global.baseapp = global.basedir + '/app/'
global.baseup  = global.basedir + '/upload/'
global.baseup_img = global.baseup + 'image/'
global.baseup_file = global.baseup + 'file/'

global.pri            = require(global.baseapp + 'include/rxPrivate.js')
global.rxu            = require(global.baseapp + 'include/rxUtil.js')
global.rxController   = require(global.baseapp + 'include/rxController.js')
global.rxError        = require(global.baseapp + 'include/rxError.js')
global.rxDatabase     = require(global.baseapp + 'include/rxDatabase.js')
// global.rxCronjob      = require(global.baseapp + 'include/rxCronjob.js')

let requestHandler  = require(global.baseapp + 'modules/requestHandler')
let checkPermission = require(global.baseapp + 'modules/checkPermission')

let rxapp = require(global.baseapp + 'rxapp')
rxapp.init()

// Inject modules
rxapp.use(checkPermission)
rxapp.use(requestHandler)

rxapp.listen(pri.serverconf.port)
console.log('App running at port: ' + pri.serverconf.port)
